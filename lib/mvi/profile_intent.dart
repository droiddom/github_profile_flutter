import 'package:github_profile_flutter/mvi/profile_view_model.dart';
import 'package:github_profile_flutter/network/graphQl/graphql_client.dart';
import 'package:rxdart/rxdart.dart';

abstract class ProfileIntentInt{

  void loadProfileInfo(String username);
  Stream<ProfileViewModel> getProfileVMObservable();
}

class ProfileIntent implements ProfileIntentInt{

  final GraphQlClientInterface _graphQlClientInterface;
  final PublishSubject<ProfileViewModel> _subject = PublishSubject();

  ProfileIntent(this._graphQlClientInterface);



  @override
  void loadProfileInfo(String username) {
    // TODO: implement loadProfileInfo
  }

  @override
  Stream<ProfileViewModel> getProfileVMObservable() {
    // TODO: implement getProfileVMObservable
    throw UnimplementedError();
  }


}