
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileDescription extends StatelessWidget {
  final userAvatar;
  final username;
  final userLogin;

  const ProfileDescription({
    required this.userAvatar,
    required this.username,
    required this.userLogin,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          radius: 40,
          backgroundImage: NetworkImage(userAvatar),
          backgroundColor: Colors.transparent,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Text(username,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Text(userLogin),
            ),
          ],
        ),
      ],
    );
  }
}

class FollowersAndFollowing extends StatelessWidget {
  const FollowersAndFollowing(
      {Key? key, this.followersCount, this.followingCount})
      : super(key: key);

  final followersCount;
  final followingCount;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(followersCount.toString(),style: const TextStyle(fontWeight: FontWeight.bold),),
        const SizedBox(width: 8,),
        const Text("followers"),
        const SizedBox(width: 32,),
        Text(followingCount.toString(),style: const TextStyle(fontWeight: FontWeight.bold),),
        const SizedBox(width: 8,),
        const Text("following"),
      ],
    );
  }
}

class RepositoryType extends StatelessWidget {
  final repoType;

  const RepositoryType({required this.repoType});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(repoType,style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
        const Text("View all",style: TextStyle(fontWeight: FontWeight.bold,decoration: TextDecoration.underline),),
      ],
    );
  }
}

class RepoItem extends StatelessWidget {
  final avatarUrl;
  final userlogin;
  final repoName;
  final repoDescription;
  final starCount;
  final languageName;
  final languageColor;

  const RepoItem({
    required this.avatarUrl,
    required this.userlogin,
    required this.repoName,
    required this.repoDescription,
    required this.starCount,
    required this.languageName,
    required this.languageColor,
  });

  @override
  Widget build(BuildContext context) {

    String color = languageColor.replaceAll('#', '0xff');
    final colorInt = int.parse(color);

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                CircleAvatar(
                  radius: 20,
                  backgroundImage: NetworkImage(avatarUrl),
                  backgroundColor: Colors.transparent,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(userlogin),
                ),
              ],
            ),
            Text(repoName,style: const TextStyle(fontWeight: FontWeight.bold),),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Text(repoDescription,overflow: TextOverflow.ellipsis ,),
            ),
            Row(
              children: [
                const Icon(Icons.star,color: Colors.yellow,),
                const SizedBox(width: 4,),
                SizedBox(child: Text(starCount),width: 50,),
                Icon(Icons.circle,color: Color(colorInt),),
                const SizedBox(width: 8,),
                Flexible(child: Text(languageName,overflow: TextOverflow.ellipsis,)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
