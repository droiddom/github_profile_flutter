import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:github_profile_flutter/network/graphQl/profile_data.dart';
import 'package:github_profile_flutter/screen/profile_widgets.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProfileView extends StatelessWidget {
  final Profile profile;

  const ProfileView({Key? key, required this.profile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProfileContainer(profile: profile);
  }
}

class ProfileContainer extends StatefulWidget {
  const ProfileContainer({Key? key, required this.profile}) : super(key: key);
  final Profile profile;

  @override
  State<StatefulWidget> createState() {
    return ProfileContainerState();
  }
}

class ProfileContainerState extends State<ProfileContainer>{
  late Profile profile;

  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    profile = widget.profile;
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SmartRefresher(
        onRefresh: _onRefresh,
        enablePullDown: true,
        enablePullUp: false,
        header: const WaterDropHeader(),
        controller: _refreshController,
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Center(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("Profile",style: TextStyle(fontSize: 24,fontWeight: FontWeight.bold),),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: ProfileDescription(
                      userAvatar: profile.avatarUrl,
                      username: profile.name,
                      userLogin: profile.login,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 16, 8, 0),
                    child: Text(profile.email),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 16),
                    child: FollowersAndFollowing(
                      followersCount: profile.followersCount,
                      followingCount: profile.followingCount,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: RepositoryType(repoType: "Pinned"),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: SizedBox(
                      height: 300,
                      child: ListView.builder(
                        itemCount: profile.pinnedRepositories.length,
                        itemBuilder: (context, index) {
                          final item = profile.pinnedRepositories[index];

                          return RepoItem(
                            avatarUrl: item.ownerAvatarUrl,
                            userlogin: item.ownerLogin,
                            repoName: item.repoName,
                            repoDescription: item.description,
                            starCount: item.stargazerCount.toString(),
                            languageName: item.primaryLanguage,
                            languageColor: item.languageColor,
                          );
                        },
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(8,16,8,0),
                    child: RepositoryType(repoType: "Top Repositories"),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: SizedBox(
                      height: 150,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: profile.topRepositories.length,
                        itemBuilder: (context, index) {
                          final item = profile.topRepositories[index];

                          return SizedBox(
                            width: 200,
                            height: 150,
                            child: RepoItem(
                              avatarUrl: item.ownerAvatarUrl,
                              userlogin: item.ownerLogin,
                              repoName: item.repoName,
                              repoDescription: item.description,
                              starCount: item.stargazerCount.toString(),
                              languageName: item.primaryLanguage,
                              languageColor: item.languageColor,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(8,16,8,0),
                    child: RepositoryType(repoType: "Started Repositories"),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: SizedBox(
                      height: 150,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: profile.starredRepositories.length,
                        itemBuilder: (context, index) {
                          final item = profile.starredRepositories[index];

                          return SizedBox(
                            width: 200,
                            height: 150,
                            child: RepoItem(
                              avatarUrl: item.ownerAvatarUrl,
                              userlogin: item.ownerLogin,
                              repoName: item.repoName,
                              repoDescription: item.description,
                              starCount: item.stargazerCount.toString(),
                              languageName: item.primaryLanguage,
                              languageColor: item.languageColor,
                            ),
                          );
                        },
                      ),
                    ),
                  ),

                ],
              ),
            )
          ],
        ),
      ),
    );
  }



  void _onRefresh() async{

    await Future.delayed(const Duration(milliseconds: 1000));
    _refreshController.refreshCompleted();
/*
    setState(() {
    });*/
  }
}
