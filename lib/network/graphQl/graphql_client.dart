import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

abstract class GraphQlClientInterface {
  ValueNotifier<GraphQLClient> getGraphQLClient();
}

class GraphQlClient implements GraphQlClientInterface {

  final String _url;
  final String _token;

  late final HttpLink _httpLink;
  late final AuthLink _authLink;
  late final Link _link;

  late final ValueNotifier<GraphQLClient> client;

  GraphQlClient(this._url, this._token) {
    _httpLink = HttpLink(
      _url,
    );

    _authLink = AuthLink(
      getToken: () async => _token,
    );

    _link = _authLink.concat(_httpLink);

    client = ValueNotifier(
      GraphQLClient(
          link: _link,
          cache: GraphQLCache(store: HiveStore()),
          defaultPolicies:
              DefaultPolicies(query: Policies(fetch: FetchPolicy.cacheFirst))),
    );
  }

  @override
  ValueNotifier<GraphQLClient> getGraphQLClient() {
    return client;
  }
}
