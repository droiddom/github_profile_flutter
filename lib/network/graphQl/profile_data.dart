class Profile {
  final String name;
  final String? avatarUrl;
  final String login;
  final String email;
  final int followersCount;
  final int followingCount;

  final List<Repository> pinnedRepositories;
  final List<Repository> topRepositories;
  final List<Repository> starredRepositories;

  Profile({
    required this.name,
    required this.avatarUrl,
    required this.login,
    required this.email,
    required this.followersCount,
    required this.followingCount,
    required this.pinnedRepositories,
    required this.topRepositories,
    required this.starredRepositories,
  });

  factory Profile.fromMap(Map<String, dynamic> result) {
    final user = result["user"];

    List pinnedRepoNodes = user["pinnedItems"]["nodes"];
    List<Repository> pinnedRepos =
        pinnedRepoNodes.map((e) => Repository.fromMap(e)).toList();

    List topRepoNodes = user["topRepositories"]["nodes"];
    List<Repository> topRepos =
        topRepoNodes.map((e) => Repository.fromMap(e)).toList();

    List starredRepoNodes = user["starredRepositories"]["nodes"];
    List<Repository> starredRepos =
        starredRepoNodes.map((e) => Repository.fromMap(e)).toList();


    return Profile(
        name: user["name"],
        avatarUrl: user["avatarUrl"],
        login: user["login"],
        email: user["email"],
        followersCount: user["followers"]["totalCount"],
        followingCount: user["following"]["totalCount"],
        pinnedRepositories: pinnedRepos,
        topRepositories: topRepos,
        starredRepositories: starredRepos);
  }


}

class Repository {
  final String repoName;
  final String ownerLogin;
  final String? ownerAvatarUrl;
  final String? description;
  final int? stargazerCount;
  final String? primaryLanguage;
  final String? languageColor;

  Repository(
      {required this.repoName,
      required this.ownerLogin,
      required this.ownerAvatarUrl,
      required this.description,
      required this.stargazerCount,
      required this.primaryLanguage,
      required this.languageColor});

  factory Repository.fromMap(Map<String, dynamic> repositoryMap) {
    final repo = Repository(
        repoName: repositoryMap["name"],
        ownerLogin: repositoryMap["owner"]?["login"],
        ownerAvatarUrl: repositoryMap["owner"]?["avatarUrl"],
        description: repositoryMap["description"] ?? "",
        stargazerCount: repositoryMap["stargazerCount"],
        primaryLanguage: repositoryMap["primaryLanguage"]?["name"],
        languageColor: repositoryMap["primaryLanguage"]?["color"]);
    return repo;
  }
}
