const readRepositories = """
  query ReadRepositories(\$nRepositories: Int!) {
    viewer {
      repositories(last: \$nRepositories) {
        nodes {
          id
          name
          viewerHasStarred
        }
      }
    }
  }
""";

const readRepositories1 = """
  query GetRepository {
  repository(owner: "facebook", name: "react"){
    id
    createdAt
  }
}
""";

const readProfile = """ 
  query GetUserProfile{
	user(login:"JakeWharton"){
  	 name
    avatarUrl
    login
    email
    followers(first:1){
      totalCount
    }
    following(first:1){
      totalCount
    }
    pinnedItems(first:3,types:REPOSITORY){
			 nodes{
        ... on Repository{
          name
          owner{
            avatarUrl
            login
          }
          description
          stargazerCount
          primaryLanguage{
            name
            color
          }
        }
      }
    }
    
    topRepositories(first:10,orderBy:{field:UPDATED_AT,direction:DESC}){
      nodes{
        ... on Repository{
          name
          owner{
            avatarUrl
            login
          }
          description
          stargazerCount
          primaryLanguage{
            name
            color
          }
        }
      }
    }
    
    starredRepositories(first:10){
      nodes{
        ... on Repository{
          name
          owner{
            avatarUrl
            login
          }
          description
          stargazerCount
          primaryLanguage{
            name
            color
          }
        }
      }
    }
    
  }
}
""";
