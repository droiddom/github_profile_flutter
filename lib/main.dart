import 'package:flutter/material.dart';
import 'package:github_profile_flutter/network/graphQl/graphql_client.dart';
import 'package:github_profile_flutter/network/graphQl/profile_data.dart';
import 'package:github_profile_flutter/network/graphQl/query.dart';
import 'package:github_profile_flutter/screen/profile.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

void main() async {
  await initHiveForFlutter();

  // const url = "https://api.github.com/graphql";
  // const token = 'Bearer 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';
  //
  /*final mainApp = MultiProvider(
    providers: [
      Provider<GraphQlClientInterface>(create: (_) => GraphQlClient(url, token))
    ],
  );*/

  //

  final HttpLink httpLink = HttpLink(
    'https://api.github.com/graphql',
  );

  final AuthLink authLink = AuthLink(
    getToken: () async => 'Bearer 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',
  );

  final Link link = authLink.concat(httpLink);

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
        link: link,
        cache: GraphQLCache(store: HiveStore()),
        defaultPolicies:
            DefaultPolicies(query: Policies(fetch: FetchPolicy.cacheFirst))),
  );

  runApp(MyApp(client));
}

class MyApp extends StatelessWidget {
  const MyApp(this.client, {Key? key}) : super(key: key);

  final ValueNotifier<GraphQLClient> client;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: MaterialApp(
          title: 'Profile',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Scaffold(
            body: Query(
              options: QueryOptions(
                  document: gql(readProfile),
                  pollInterval: const Duration(minutes: 10)),
              builder: (
                QueryResult result, {
                Refetch? refetch,
                FetchMore? fetchMore,
              }) {
                if (result.hasException) {
                  return Text(result.exception.toString());
                }

                if (result.isLoading) {
                  return const Text("Loading");
                }

                if (result.data == null) {
                  return const Text("Profile Not Available");
                } else {
                  Profile profile = Profile.fromMap(result.data!);

                  return ProfileView(profile: profile);
                }
              },
            ),
          )),
    );
  }
}
